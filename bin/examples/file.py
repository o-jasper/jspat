import os

from JSPat.Server import RequestHandler, Server

# TODO this is not good.. now people have to put it there or modify.
base_dir = os.getenv('HOME') + "/proj/JSPat/"

from JSPat.handler.JSPatPage import JSPatPage

from JSPat.obj.File import File
from JSPat.obj.C404 import C404

def fileserv(p, **query):
    return File('/' + '/'.join(p[1:]))

def subfileserv(p, **query):
    return File('/' + '/'.join(p[1:]), 'subobj_page')

jspat = JSPatPage(assets=[base_dir + "assets/"], JSPat_dir=base_dir + "js/")

serv = Server(('', 10000), RequestHandler,
              dict={'js':(jspat.serve_JSPat_asset, lambda p,**q:p),
                    'file':(jspat_page, fileserv),
                    'subfile':(jspat, subfileserv),
                    'default':(jspat, C404)},
              )

serv.serve_forever(0.5)
