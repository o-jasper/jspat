import os
base_dir = os.getenv('HOME') + "/proj/JSPat/"  # TODO assumes location...

from JSPat.handler.JSPatPage import JSPatPage

jspat = JSPatPage(assets=[base_dir + "assets/", "/tmp/jspat/"], JSPat_dir=base_dir + "js/")

from JSPat.obj.File import File
from JSPat.obj.C404 import C404

def fileserv(p, **query):
    return File('/' + '/'.join(p))

def subfileserv(p, **query):
    return File('/' + '/'.join(p), 'subobj_page')

def path_as_str(p, **q):
    return '/'.join(p)

obj_dict = {  # 'key':(handler, obj)
    # `strip_path` produces string which `.serve_asset` sees as object to handle.
    'assets': (jspat.serve_asset,       path_as_str),
    'js':     (jspat.serve_JSPat_asset, lambda p,**q: p),

    # From non-callable handlers, it figures it out itself.
    'file':   (jspat, fileserv),
    'subfile':(jspat, subfileserv),
    'default':(jspat, C404),
    'base_dir':(jspat, File(base_dir))  # Example of instance as object instead of callable.
}

# Run it.
from JSPat.Server import RequestHandler, Server

serv = Server(('', 10000), RequestHandler, dict=obj_dict)

serv.serve_forever(0.5)
