import os

# TODO this is not good.. now people have to put it there or modify.
base_dir = os.getenv('HOME') + "/proj/JSPat/"

from JSPat.handler.JSPatPage import JSPatPage

from JSPat.obj.File import File
from JSPat.obj.C404 import C404

def fileserv(p, **query):
    return File('/' + '/'.join(p))

def subfileserv(p, **query):
    return File('/' + '/'.join(p), 'subobj_page')

jspat = JSPatPage(assets=[base_dir + "assets/", "/tmp/jspat/"], JSPat_dir=base_dir + "js/")

def serve_format(string, **kv):
    def serve(handler, file, **q):
        q.update(kv)
        handler.serve_format(200, string, **q)
    return serve

obj_dict = {
    'assets':jspat.serve_asset, 'js':jspat.serve_JSPat_asset,
    'file':(jspat, fileserv), 'subfile':(jspat, subfileserv),
    'default':(jspat, C404),

    'base_dir':(jspat, File(base_dir))
}

def fail(name, msg, **kv):
    print(("{name}:" + msg).format_map({'name':name, **kv}))
    obj_dict[name] = (serve_format("{name}:"+msg, name=name, **kv), msg)

lst = []
from JSPat.obj.auto import auto

print("Iris plot and dataframe.")  ### ---
try:
    import pandas as pd

    from sklearn.datasets import load_iris
    iris = load_iris()
    iris_df = pd.DataFrame(data=iris['data'], columns=iris['feature_names'])
    iris_df['target'] = [iris['target_names'][ti] for ti in iris['target']]

    obj_dict['iris'] = (jspat, auto(iris_df))

#    obj_dict['iris_plot'] = (jspat,  # Oddly broken.. cloudpickle.py? Qt? don't get it..
#                             auto(iris_df.plot.scatter('sepal length (cm)', 'sepal width (cm)')))

except BaseException as e:
    fail('iris', "Couldn't get the iris DataFrame.\n{err}", err=e)

print("Basic Graphviz graph.")  ### ---
try:
    import graphviz

    g = graphviz.Digraph()
    for s in "a:b|b:c|c:a|b:d".split('|'): g.edge(*s.split(':'))

    obj_dict['graphviz'] = (jspat, auto(g))

except BaseException as e:
    fail('graphviz', "Couldn't get the graphiz Dot object.\n{err}\n", err=e)

from JSPat.Server import memoizing

print("Feeds manager.")  ### ---
if True:
    from feedsmanager import FeedsManager, FeedEntryDict, FeedMetaDict
    from pathlib import Path

    Path("feeds").mkdir(exist_ok=True)

    fm = FeedsManager("/tmp/feeds/")

    def feed_from(p):
        f = fm.feed('/'.join(p.split('/')[2:]))
        f.rake_in()
        return auto(f)
    obj_dict['feedsmanager'] = (jspat, memoizing(feed_from))

#except BaseException as e:
#    fail('feedsmanager', "Feedsmanager failed somehow.\n{err}\n", err=e)

print("Python interpreter")  ### ---
try:
    from JSPat.obj.Py import Py

    obj_dict['py'] = (jspat, Py("1+2"))
except BaseException as e:
    fail('python interpreter', "Python interpreter failed somehow\n{err}\n", err=e)

obj_dict['str'] = (jspat, auto("just a string for test"))
print(obj_dict['str'][1])

fail('fail_on_purpose', "Fail string on purpose.. not actual exception shower yet..")

print("list of some of those.")  ### ---
obj_dict['list'] = (jspat, auto(
    (obj_dict[k][1] for k in
     ['iris_plot', 'iris', 'graphviz', 'base_dir', 'str', 'py', 'fail_on_purpose']
     if k in obj_dict)))

from JSPat.Server import RequestHandler, Server

serv = Server(('', 10000), RequestHandler, dict=obj_dict)

serv.add_page('server', jspat, auto(serv))

serv.serve_forever(0.5)
