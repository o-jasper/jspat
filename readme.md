
# Object - HTML-page correspondences
With main concept that each class of objects has multiple templates & data-to-fill-them-in.

It can do the filling in part both entirely server-side(&redirect `<noscript>`) and client-side.
A bit of javascript can calculate some values.(save bandwidth) A value can be pointed out as
key-value so the web page can still access the server-side object.

Templates can place a list of other objects the object can output.
It's detected beforehand if this is needed and send as a block; no RPC-calls back and forth.
Few of them, no wasteful calls, etcetera/

It's intended to make objects quickly UI-able, and make it snappy & bandwidth efficient.
Although I expect to add "manual" RPC calls as option aswel.
(even though I hope such use will be atypical)

### Install
Symlink from `~/.local/lib/python3.7` to `./JSPat/` for instance(linux)

    # from this directory.
    cd ~/.local/lib/python3.7/
    ln -s $OLDPWD/JSPat

**TODO** pip package.

### Run
This runs a bunch of things.
(uses `try:..except` and may not do things depending on available dependencies)

    # from this directory.
    python bin/examples/all.py  # (or file.py, readme.py)

    firefox http://localhost:10000/list

### Develop
Use the examples. For instance `JSPat/obj/File.py` and `bin/examples/file.py`

Make Python classes like in `JSPat/obj/` methods:

#### Components

`jspat_accessible` is a list of names of components it may `getattr` out of the object.
(`@property` components should work)

`jscall_{name}` is snippets of javascript used to calculate components on te the browser end.

`jspat_by_key` is the component that will be used to address your object, if it is inside
another object. It can also use `jscall_..`.

#### Templates
`.template_<template_name>` a list of strings.
* First element such as `div.htm`, before the dot is used as html-tagname. After currently ignored.
* Rest is a list of strings containing the templates.

For instance `template_plain_text = ['text', "Just some plain text, {%h|3|templates apply}"]`

(you'll probably want at least `.template_page`)

You can also put these in separate files, in a directory named after the class.
It should work according to class derivation, also the files.

#### Sub lists
Only need defining when used of course.
`.jspat_subgen_{sublist_name}(self, args)` returns a generator making objects.

`{%sublist|div|dirlist|entry|arg1|arg2}` will end up calling `.jstpat_subgen_dirlist` with

`args` having the value `["arg1", "arg2"]`
it will use the `.template_entry` *of the output types*.(because it says entry there)

Note that it scans the templates to see which sublists will be needed ahead of time, and they
will be sent immediately. Here, the arguments can't be outputs from other macros.

#### Optional other info; `.jspat_info(self, **query)`
returns a dictionary of info if served has the whole page. If not specified all the defaults are used.

+ `'title'`, the title, defaultly `"JSPAT: " + path`.

+ `'httpcode'` for the http code, default `200`.

### Using
The main this is a dictionary of pairs. The keys are the first directory,
and the two values are is the handler and the object.

If the handler is a function it will use that straight up, otherwise, it will figure it out.
`JSPatPage` objects are sufficient if the object is one of the above.

The objects can be functions with the path(as list) and query, in which case they'll be called
to get the actual object.

Below an example, also available in `bin/examples/readme.py`.

    import os
    base_dir = os.getenv('HOME') + "/proj/JSPat/"  # TODO assumes location...

    from JSPat.handler.JSPatPage import JSPatPage

    jspat = JSPatPage(assets=[base_dir + "assets/", "/tmp/jspat/"], JSPat_dir=base_dir + "js/")

    from JSPat.obj.File import File
    from JSPat.obj.C404 import C404

    def fileserv(p, **query):
        return File('/' + '/'.join(p))

    def subfileserv(p, **query):
        return File('/' + '/'.join(p), 'subobj_page')

    def path_as_str(p, **q):
        return '/'.join(p)

    obj_dict = {  # 'key':(handler, obj)
        # `strip_path` produces string which `.serve_asset` sees as object to handle.
        'assets': (jspat.serve_asset,       path_as_str),
        'js':     (jspat.serve_JSPat_asset, path_as_str),

        # From non-callable handlers, it figures it out itself.
        'file':   (jspat, fileserv),
        'subfile':(jspat, subfileserv),
        'default':(jspat, C404),
        'base_dir':(jspat, File(base_dir))  # Example of instance as object instead of callable.
    }

    # Run it.
    from JSPat.Server import RequestHandler, Server

    serv = Server(('', 10000), RequestHandler, dict=obj_dict)

    serv.serve_forever(0.5)


The `assets` and `js`one has that `strip_path` basically it returns a string object on which
`jspat.serv_assets` works to  give the correct asets. If it's being used with string,
it will need the `jspat.serve_JSPat_asset` for `'js'` too.

With `fileserv`, `subfileserv`  and `C404 it is working on callables, but it can work with
instances as the bottom one shows.

#### Templates
Templates always in the form `{%name|arg1|arg2|arg3|...|argn}` with any number of arguments.

There are just a few right now, in future, hopefully more and developer-definable.

* `{%c|component_name}` gets that component name out of the data.

* `{%sublist|tag_name|call_name|template_name|arguments...}` output of a generator
  `.jspat_subgen_{call_name}(["arg1", "arg2", ...])` into a
  `<tag_name></tag_name>` where it will use `.asset_{template_name}`.

* It has some other stuff like `{%h|..}` and `{%a|...}` but currently bit few things.

Can try them somewhat in `js/example/path.html`. It will list the needed components
(`need_c`) and needed sublists

### Seen from a browser:

* Noscript pages are shown initially largely same without javascript.

  *TODO* is to have something that can still do the forms.

* With noscript, it sends the pages(obviously) otherwise sends just bits of template and data.

  *TODO* versioning & using cache, and some kind of cache, if possible, for templates.

* `&mode=noscript` forces noscript, `&mode=jspage` embeds the data into the page and
  `&mode=rpc` sends a single RPC call on start up; Forms for both the latter send an RPC call
  for each form, and possibly in object-defined RPC-calls defined in the future.

  Defaultly `JSPatPage.default_mode`; `'rpc'`

* `&template=page` is the template to view.

* Other query arguments go to the sublist functions of the top object.

## Author
Jasper den Ouden (http://ojasper.nl/)

### License: AGPL

See `doc/agpl-3.0.txt`.

## TODO

* Have forms, they need better behaviors & keyboard navigation.

* Plain RPC to the objects.

* Loading javascript & css from the templates.

  + Basic permissions regarding the objects.(i.e. indicate that no extra javascript is loaded)

* Option for more compact data, [BSON](http://bsonspec.org/), or even types on object data sent.

* Maybe basic website where macros gather info like titles, tags, categories, citations/notes,
  other. Some of these might be "implied datapoints" too..

  + Help user with "compile static site" tool.

* Permit assets to be sent via a different url/server.

* Versioning an caching directives in the headers of (javascript)files.

  + Putting templates in javascript and caching that way is an option too..

* Userscript/addon client?
  + Caching and versioning of templates.
  + Perhaps a system storing the data entries too. Will need whole concept of what's already there
    and what is not.

  Even just a raw GTK client?.. Replace HTML with templates that it knows how to turn into
  widgets.

* Using `{%form|into_,..|content}` and `{%sublist` has a bit much mental overhead..

* More testing.

## Got (not to say done..)
* Defaultly make browser use one RPC call to get the data, keep the current option.
* Ability to put in forms that can submit and then prepend/append/replace(other?)
* Permit objects to add javascript functions to add template. (and maybe calculate fields)
