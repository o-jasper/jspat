#  Copyright (C) 16-01-2019 Jasper den Ouden.
#
#  This is free software: you can redistribute it and/or modify
#  it under the terms of the Affero GNU General Public License as published
#  by the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.

from pathlib import Path

def path_type(path):
    i = path.rfind("/")
    i = path.find(".", 0 if i<0 else i)
    return 'html' if i<0 else path[i:]

class Assets:
    """List of asset directories. Has methods to find the
actual assets.

WARN: instance of this class  may "read as false" when it has zero directories.
"""
    if_no_tp = 'html'

    def __init__(self, assets=None, **kv):
        self.assets = assets or []

    # TODO maybe tuple-ize file types or something? Use Path object better?
    def full_path(self, path, tp=None):
        """Full path and the type."""
        if not tp:
            i = path.find('.', max(0,path.rfind('/')))
            tp = path[i+1:] if i>=0 else self.if_no_tp

        for d in self.assets:  # Either straight or with /index.<type>
            f = d + "/" + path
            if Path(f).is_file():  # TODO $PATH/index.html and $PATH/ to $PATH
                return f, tp
            elif Path(f).is_dir() and tp:
                if Path(f + "/index." + tp).is_file():
                    return f + "/index." + tp, tp
                elif Path(f + "/index.pat." + tp).is_file():
                    return f + "/index.pat." + tp, "pat." + tp
            elif Path(f + "." + tp).is_file():
                return f + "." + tp, tp
            elif Path(f + ".pat." + tp).is_file():
                return f + ".pat." + tp, "pat." + tp
            else:
                f = f[:-len(tp)] + "pat." + tp
                if Path(f).is_file():
                    return f, "pat." + tp

        return None, tp  # Failed.

    def yield_asset(self, fpath, tp=None, not_found_tp=None):
        """Open the file and return `.readlines`(assume it acts as an iterable)
and thought on file type."""
        path, tp = self.full_path(fpath, tp)
        if path:
            with open(path) as fd: return fd.readlines(), tp
        else:
            return ["Could not find ", fpath, " (", str(tp), ')'], not_found_tp or tp

    def serve_asset(self, handler, _by_name, plst, query):
        """Serve the plain asset."""
        path, tp = self.full_path('/'.join(plst), query.get('tp', None))
        if path:
            handler.serve_file(200, tp, path)
        else:
            handler.serve_format(404, "console.log(\"Couldnt find file; {file}\",\n", file=file)

    handle_get = serve_asset
