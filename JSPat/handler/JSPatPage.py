#  Copyright (C) 16-01-2019 Jasper den Ouden.
#
#  This is free software: you can redistribute it and/or modify
#  it under the terms of the Affero GNU General Public License as published
#  by the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.

from JSPat.handler.JSPat import JSPat, SendOneRecipient, fname_of, JSONCmdSend

def down_path(obj, path):
    """Use obj,path = obj.down_path((path) until it indicates it is finished. """
    finished = False
    while finished:
        fun = getattr(obj, 'down_path', None)
        if fun is None:
            return obj, path
        obj,path, finished = fun(path)
    return obj, path

def fixlen(lst, defaults):
    """Always return the same number of results, using defaults if needed."""
    for i,e in enumerate(lst):
        yield e
        if i+1 >= len(defaults): return
    for j in range(i,len(defaults)):
        yield defaults[i]

def script_html(version, lst):
    return ''.join("<script src=\"/js/{}/{}\"></script>\n".format(version, f) for f in lst)

class JSPatPage(JSPat):

    browser_js = ["form.js", "msg.js", "event_bindings.js", "nav.js"]

    def serve_subgen(self, handler, _got_name, obj, query):
        """Sends sublists accessing object based on `query['path']` and `query['sublists']`"""
        handler._type_header(200, 'text/json')

        wfile = handler.wfile
        wfile.write(bytes("[\n", 'utf'))
        self.raw_serve_subgen(JSONCmdSend(wfile, True), obj, query)
        wfile.write(bytes("\n]", 'utf'))

    handle_post = serve_subgen

    def raw_serve_subgen(self, cmdsend, obj, query):
        path, sublists = query.get('path', None), query.get('sublists', None)
        if sublists is None:
            return cmdsend.cmd('error', "Sublists to generate lists from not specified.")

        if path:  # Go down the given path, if one is availabke.
            obj,path = down_path(obj, path)
            query['path'] = path

        sender = self.sender(self, cmdsend)
        for i, sub in enumerate(sublists.split(',')):
            if i > 0: cmdsend.cmd(',')  # Splits different answers.
            subgen_name, sub_pat_name = fixlen(sub.split(':'), ['main', 'inline'])
            sender.send_subgen(obj, fname_of(type(obj), sub_pat_name), subgen_name, sub_pat_name,
                               query.get('args', ""), query)

    default_mode = 'rpc'

    def handle_page(self, handler, got_name, obj, query):
        """Like JSPat.handle, but instead of just dealing the json data,
deals a whole page to show it in browsers."""
        try:
            obj.jspat_path = got_name
        except:
            pass

        mode = query.get('mode', self.default_mode)

        info = getattr(obj, 'jspat_info', lambda **q:{})(**query)
        info['title'] = info.get('title', "JSPAT: " + handler.path)

        template_name, httpcode = query.get('template', 'page'), info.get('httpcode', 200)
        wfile = handler.wfile

        if mode == 'json':  # Just the json data.
            handler._type_header(httpcode, 'text/json')
            return self.handle(wfile, obj, template_name, query)
        elif mode == 'subgen_json': # JSON data, specific ask.
            return self.serve_subgen(handler, got_name, obj, query)

        handler._type_header(httpcode, 'text/html')  # Going to be text/html.

        header_part = """<!doctype html><html><head><meta charset="utf-8">
<title>{title}</title>
""".format_map(info)
        wfile.write(bytes(header_part, 'utf'))
        if mode != 'noscript':  # Write scripts and noscript redirector.
            wfile.write(bytes(script_html(self.version, self.need_js + self.browser_js), 'utf'))
            wfile.write(bytes(
                """<noscript><meta http-equiv="refresh" content="0;url=?mode=noscript"></noscript>""", 'utf'))
        wfile.write(bytes("</head><body>\n", 'utf'))

        # Getting directly.
        if mode == 'noscript':  # Run the javascript server-side.
            fname = type(obj).__module__[16:].replace('.', '/') + "/" + template_name
            _needs, _sublists, ret = self.expand_serverside(fname, obj, template_name, query)
            wfile.write(bytes("".join(ret[1:]), 'utf'))
        elif mode == 'rpc':    # Data asked for with javascript.
            wfile.write(bytes("""<div id="main" class="sublist st_main"></div><script>
var url = document.URL;
var use_url = url.substr(0, url.search("\\\\?")) + "\\?mode=json";

glob_data = init_data({'into_el':ge('main'), 'into_mode':'a'})

msg_get(use_url, function(got) { obtain_1(got, glob_funs, glob_data) })
</script>""", 'utf'))
        elif mode == 'jspage':  # Data sent in bit of javascript.
            wfile.write(bytes("""<div id="main" class="st_main"></div>
<script>top_data=[""", 'utf'))

            sender = self.sender(self, JSONCmdSend(wfile, True))
            sender.send_obj(obj, template_name, query)

            wfile.write(bytes("""]
glob_data = init_data({'into_el':ge('main'), 'into_mode':'a'})
for(var i=0 ; i < top_data.length ; i++) {
  obtain_1(top_data[i], glob_funs, glob_data)
}</script>""", 'utf'))
        else:
            wfile.write(bytes("Unrecognized mode; {}".format(mode), 'utf'))

        wfile.write(bytes("\n</body></html>", 'utf'))

    handle_get = handle_page
