#  Copyright (C) 16-01-2019 Jasper den Ouden.
#
#  This is free software: you can redistribute it and/or modify
#  it under the terms of the Affero GNU General Public License as published
#  by the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.

from JSPat.handler.Assets import Assets

import subprocess, json

def try_json_loads(inp):
    try:
        return json.loads(inp)
    except BaseException as e:
        raise BaseException(
            "JSON loading in JSPat failed, got:\n{}'||\nErr:\n{}\n".format(inp, e))

class JSProg:
    """TODO make single continuous running program, or use a library for it.
(should be able to make different approaches interchangable)"""
    def __init__(self, need_js, dir):
        self.args = ['js52']
        for f in need_js:
            self.args += ["-f", dir + f]
        self.args += ["-e"]

    def step(self, code):  # TODO think the code can get to long..
        prog = subprocess.Popen(self.args + [code],
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        err = prog.stderr.readlines()
        if len(err)>0:
            raise BaseException("JS error: {}\n<br>{}".format(
                prog.args, "".join(map(bytes.decode, err))))
        return try_json_loads("".join(map(bytes.decode, prog.stdout.readlines())))

def fname_of(cls, name):
    return cls.__name__ + '/' + name

class ListCmdSend(list):
    def cmd(self, *v): self.append(v)

class EvaluatedTemplate:
    """Class so got evaluated template information conveniently together."""
    def __init__(self, tag, template, cls, name, need_template, need_call, need_c, sublists):
        self.tag, self.cls, self.name = tag, cls, name
        self.need_template, self.need_call, self.need_c = need_template, need_call, need_c
        self.sublists = sublists

        self.template = template

        self._cmd, self.sent = None, None

    @property
    def fname(self): return fname_of(self.cls, self.name)

    def ensure_cmd(self, handler):
        """Ensures the commands to send are logged."""
        if self._cmd is None:
            self._cmd = ListCmdSemd
            SendOneRecipient(handler, self._cmd).send_template(self)
            self.sent = sender.sent
        return self._cmd

class JSONCmdSend:
    def __init__(self, wfile, wfirst=False):
        self.wfile = wfile
        self.cmd = self.maybe_first_cmd if wfirst else self.later_cmd
        self.first = wfirst

        self.write = self.wfile.write

    def maybe_first_cmd(self, *vals):
        """If there is a first, this will be used on it, but _not_only_ first"""
        self.write(bytes(("" if self.first else ",\n") + json.dumps(list(vals)), 'utf'))
        self.first = False
        self.cmd = self.later_cmd

    def later_cmd(self, *vals):
        self.write(bytes(",\n" + json.dumps(list(vals)), 'utf'))

class SendOneRecipient:
    """Sending to one recipient. Keeps track of what is being sent."""

    def __init__(self, handler, cmdsend, sent=None):
        self.sent = set() if sent is None else sent
        self.handler, self.cmdsend = handler, cmdsend

    # TODO... maybe .wv just the default, and pass `wv` differently..
    # So it can distinguish from the first..

    def send_template_always(self, et):
        """Given an assets, """
        sent, handler, wv = self.sent, self.handler, self.cmdsend.cmd

        wv('s', et.fname, et.tag, et.template, et.need_c, getattr(et.cls, "jspat_by_key", ':index'))
        sent.add(et.fname)

        for an in et.need_template:  # Might need to send info on patterns inside the patterns.
            s_et = self.handler.ensure_template(et.cls, an)
            if s_et.fname not in sent:
                self.send_template_always(s_et)

        # Send the calls this pattern needs.
        # TODO but when classes derive from each other, it will send multiple copies.. TODO css?
        for cn in et.need_call:
            call_fname, code = handler.ensure_jscall(et.cls, cn)
            if call_fname and (call_fname + ".call.js" not in sent):
                sent.add(call_fname + ".call.js")
                wv('k', call_fname, code)

    def send_subgen(self, obj, fname, subgen_name, sub_template_name, args, query):
        """Generate objects from subgen."""
        for i,sub_obj in enumerate(getattr(obj, "jspat_subgen_" + subgen_name)(args, query)):
            fname = self.send_obj(sub_obj, sub_template_name, {}, initial_fname=fname)
        return fname

    def send_obj(self, obj, et, query, initial_fname=None):
        """Raw handler, calls wv to send values."""
        wv = self.cmdsend.cmd
        if type(et) == str:
            et = self.handler.ensure_template(type(obj), et)

        if initial_fname != et.fname:  # Keep track of current template used.
            if et.fname not in self.sent:  # Send template if needed.
                self.send_template_always(et)
            wv('u', et.fname)

        # Change the current object to the one used.
        accessible = getattr(obj, 'jspat_accessible', [])
        wv('c', *((getattr(obj, k) if k in accessible else obj.jspat_get(k))
                                for k in et.need_c))  #(first done)

        # Do sublists.
        fname = et.fname  # Keep track so don't keep sending `'u'`
        for subgen_name, sub_template_name, *args in et.sublists.values():
            wv('n', subgen_name, 'oa')  # Change target. NOTE: currently tied to generator.
            fname  = self.send_subgen(obj, fname, subgen_name, sub_template_name, args, query)
            wv('d')  # Back to original target.
        return fname

import datetime

class JSPat(Assets):
    """Potentially fills in patterns if `pat.` in the file type."""

    version = "DEV"

    need_js = ["common.js", "pat.js", "time_to_str.js", "val_unit.js"]

    sender = SendOneRecipient

    def __init__(self, assets=None, JSPat_dir=""):
        self.JSPat_dir = JSPat_dir
        self.json_prog = JSProg(self.need_js, JSPat_dir)

        Assets.__init__(self, assets)
        self.mem_assets = {}

        self.templates_version = datetime.datetime.now().strftime("%Y%m%d__%H%M%S")

    def serve_JSPat_asset(self, handler, _by_name, plst, query):
        """Serve, JSPat assets and template-related info under templates/."""
        if len(plst)==4 and plst[0] == 'templates':
            et = self.mem_assets.get('/'.join(plst[2:4]), None)
            wfile = handler.wfile
            if plst[1] != self.templates_version:
                wfile.write(bytes("console.log(\"Version not matched, {} vs current {} \
Either right during update or bug that makes your browser visit this\"); \
".format(plst[1], self.templates_version),'utf'))

            if et is not None:
                wfile.write(bytes("""glob_data = init_data({'into_el':ge('main'), 'into_mode':'a'})
first_data = [["n", "main", "oa"],
""", 'utf'))
                wfile.write(bytes(",\n".join(json.dumps(el) for el in et.ensure_cmd(self)), 'utf'))
                wfile.write(bytes("""
]
for(var i=0 ; i < first_data.length ; i++) {
  obtain_1(first_data[i], glob_funs, glob_data)
}
""", 'utf'))
                return

        file = '/'.join(plst[1:] if plst[0] == self.version else ['versions', *plst])
        handler.serve_file(200, "text/javascript", self.JSPat_dir + file)

    def expand_serverside(self, asset, *raw_handle_args):
        lst = ListCmdSend()

        self.sender(self, lst).send_obj(*raw_handle_args)

        return self.json_prog.step("top_data=" + json.dumps(lst) + """
cur_data = init_data({'into_el':[], 'into_mode':'a'})
for(var i=0 ; i < top_data.length ; i++) {
  obtain_1_solid(top_data[i], glob_funs, cur_data)
}
//while(cur_data.into_stack.length > 1){ obtain_1_solid(['d'], glob_funs, cur_data) }

use_ret = cur_data.into_el
//if(cur_data.into_stack.length == 1){ obtain_1_solid(['d'], glob_funs, cur_data) }

print(JSON.stringify([cur_data.needs, cur_data.sublists, use_ret]))
""")

    # TODO figure permissions at this point.
    def template_evaluate(self, cls, name, template_gen, tp):
        """Evaluate a pattern to determine what it needs."""
        template = ''.join(template_gen)  # NOTE: does not enter any object.
        need_template, need_call, need_c, sublists = self.json_prog.step("""
cur_data = init_data()
use_pat(read_template(""" + json.dumps(template) + """, 0), glob_funs, cur_data)

print(JSON.stringify([cur_data.need_template, cur_data.need_call, cur_data.need_c, cur_data.sublists]))
""")
        # Note: need_c is needed as a list eventually, that's why treated differently.
        #  Ensuring the other things are available done by `.send_template`
        need_c, need_call = set(need_c), set(need_call)  # Add the need_c of pattern-dependencies.
        for template_name in need_template:
            more_et = self.ensure_template(cls, template_name)
            need_c = need_c.union(more_et.need_c)
            sublists.update(more_et.sublists)

        by_key = getattr(cls, "jspat_by_key", ':index')
        if by_key != ':index':  # If needed, add the piece of data indicating the key.
            need_c.add(by_key)

        for c in list(need_c):  #(iterate a copy, not sure if something could happen)
            if self.ensure_jscall(cls, c)[0]:  # If a callable exists, use that instead.
                need_c.remove(c)
                need_call.add(c)

        # Note: this is stored in self.mem_assets[fname_of(cls, name) + file_postfix]
        return EvaluatedTemplate(tp.split('.')[0], template, cls, name,
                                 need_template, sorted(set(need_call)), sorted(set(need_c)),
                                 sublists)

    def asset_search(self, cls, name, code_prefix, file_postfix):
        """Searchers for the asset in class members/associated files."""
        for scls in cls.mro():  # Search in same order of Method Resolution Order.
            cfname = scls.__name__ + '/' + name
            sgen,tp = self.yield_asset(cfname + file_postfix, not_found_tp='NotFound')
            if tp != 'NotFound':  # Found it.
                return sgen, tp
            else:  # Have to look now, the regular MRO would fail to superseed the file.
                sgen = getattr(scls, code_prefix + name.split('/')[-1], None)
                if sgen is not None:  # Found on object.
                    sgen = iter(sgen)
                    tp = next(sgen)
                    return sgen, tp
        return (iter(["Of {} asset {} not found".format(cls.__name__, name)]),
                'notfound')

    def ensure_asset(self, cls, name, evaluator, code_prefix, file_postfix):
        """Gets any kind of asset."""
        key = fname_of(cls, name) + file_postfix
        ret = self.mem_assets.get(key, None)
        if ret is None:  # Not obtained yet.
            self.mem_assets[key] = 'working'

            asset_gen, tp = self.asset_search(cls, name, code_prefix, file_postfix)
            ret = evaluator(cls, name, asset_gen, tp) # if tp!='notfound' else (False, tp)
            self.mem_assets[key] = ret

        assert ret != 'working', "Presumably a loop in pattern dependencies?!\ncls:{}\nfname:{}" \
            .format(cls, key)

        return ret

    def ensure_template(self, cls, name):
        """Get jspat template(assets) specifically."""
        return self.ensure_asset(cls, name, self.template_evaluate, 'template_', "")

    def _no_eval(self, cls, name, asset_gen, tp):
        if tp == 'notfound':
            return False, fname_of(cls, name)
        else:
            return fname_of(cls, name), "".join(asset_gen)

    def ensure_jscall(self, cls, name):
        return self.ensure_asset(cls, name, self._no_eval, 'jscall_', ".call.js")
    def ensure_css(self, cls, name):
        return self.ensure_asset(cls, name, self._no_eval, 'css_', ".css")

    def handle(self, wfile, obj, template_name, query, onto_name='main', mode='oa',
               sender=None):
        """Handles a request. Suggest use `wfile` as """
        # Open up & send where it goes initially.
        wfile.write(bytes("[\n[\"n\", \"{}\", \"{}\"]".format(onto_name, mode), 'utf'))

        sender = self.sender(self, JSONCmdSend(wfile)) if sender  is None else sender
        sender.send_obj(obj, template_name, query)

        wfile.write(bytes("\n]", 'utf'))  #Finish it up.
