#  Copyright (C) 16-01-2019 Jasper den Ouden.
#
#  This is free software: you can redistribute it and/or modify
#  it under the terms of the Affero GNU General Public License as published
#  by the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.

import math, time

class NoMatch:
    def __init__(self, obj, tpstr, module_path, *rest, **kv):
        self.obj = obj
        self.tpstr = tpstr.replace('<', "&lt;").replace('>', '&gt;')
        self.rest = rest
        self.module_path = module_path

    @property
    def objstr(self): return str(self.obj)

    jspat_accessible = set(['tpstr', 'rest', 'objstr', 'module_path'])

    template_info = ["div.html", "Val: {%c|objstr}\nOf type: {%c|tpstr}<br>{%c|rest}",
                  "<br>Tried {%c|module_path}"]
    template_inline = ["div.html", "No such plottable? {%pat|info}"]
    template_entry = ["div.html", "{%pat|inline}"]
    template_page = ["div.html", "{%pat|inline}"]

class SomeError(NoMatch):
    template_inline = ["div.html", "Failed somehow? {%pat|info}"]

# TODO for auto output use versions of SuppliedHTML, Img, that add some more info on them?
from JSPat.obj.basic import List, SuppliedHTML, Img

def basename(tpstr, tp):
    return "{}_{}.{}".format(tpstr[8:-2], math.floor(time.time()*1000), tp)

def try_savefig(val, tpstr, images_dir=None, **kv):
    file = basename(tpstr, 'png')
    val.savefig(images_dir + file, bbox_inches='tight', transparent="True", pad_inches=0)
    return Img, {'src':file, 'desc':getattr(val, 'title', None)}

def try_axes(val, tpstr, **kv):
    return try_savefig(val.figure, tpstr, **kv)

def try_pipe(val, tpstr, images_dir=None, want_tp='png', graphviz_tp=None, **kv):
    file = basename(tpstr, want_tp)

    with open(images_dir + file, 'wb') as fd:
        fd.write(val.pipe(graphviz_tp or want_tp))

    return Img, {'src':file}  # TODO has description?

def try_PIL_Image(val, tpstr, images_dir=None, **kv):
    """TODO so PIL image is.."""
    file = basename(tpstr, 'png')
    val.convert('RGB').save(images_dir + file)
    return Img, {'src':file}

def try_df_supplied_html(val, tpstr, max_rows=40, max_cols=20, **kv):
    """Uses the -to-html conversion supplied by pandas.
pObviously could do significantly better by having a Table object that produces Row objects."""
    assert type(max_rows) is int and type(max_cols) is int, \
        "Max rows/cols not integer? {} {}".format(max_rows, max_cols)
    return SuppliedHTML, {}, val.to_html(max_rows=max_rows, max_cols=max_cols)

def try_list(lst, tpstr, **kv):
    return (lambda lst, **kv: List(map(auto, lst), **kv)), {}, lst

def try_str(val, tpstr, **kv):
    return lambda v, **kv: SuppliedHTML(str(v)), {}, val

def auto_FeedParserDict(val, tpstr, **kv):
    """Deals with module `feedparser` output."""
    from JSPat.obj.lib.feedparser import uiFeedWhole, uiFeedEntry

    # If it has entries, assume top level object. Not fool proof..
    return (uiFeedWhole if 'entries' in val else uiFeedEntry), {}, val


def clstr(x): return "<class '" + x + "'>"

default_handlers = {  # TODO one for exceptions?
    clstr('matplotlib.axes._subplots.AxesSubplot'): try_axes,
    clstr('matplotlib.figure.Figure'):try_savefig,
    clstr('graphviz.dot.Digraph'):try_pipe,
    clstr('geopandas.geodataframe.GeoDataFrame'):try_df_supplied_html,
    clstr('pandas.DataFrame'):try_df_supplied_html,
    clstr('pandas.core.frame.DataFrame'):try_df_supplied_html,
    clstr('PIL.Image.Image'):try_PIL_Image,
    clstr('list'):try_list,
    clstr('str'):try_str,
    clstr('generator'):try_list,
    clstr('FeedParserDict'):auto_FeedParserDict,
}

import importlib.util

from pathlib import Path

def no_match_default(obj, tpstr, **kv):
    return (NoMatch, {}, obj, tpstr, "(none)")

def auto_find(tpstr=None, obj=None, handlers=default_handlers, default_handler=no_match_default,
              memoize=False):
    """Find the function for producing the JSPat object, if any. (or get tpstr from obj)"""
    if tpstr is None:
        tpstr = str(type(obj))

    fun = handlers.get(tpstr, None)  # Finding the handler/
    if fun is not None:
        return fun
    else:
        try:
            tp_lst = tpstr[8:-2].split('.')  # TODO only sometimes works..
            module_path = tp_lst[0] + '.ui.JSPat.' + '.'.join(tp_lst[1:-1])
            spec        = importlib.util.find_spec(module_path)
            if spec is None:
                fun = lambda obj,tpstr,**kv: (NoMatch, {}, obj, tpstr, module_path)
            else:
                module = importlib.util.module_from_spec(spec)
                spec.loader.exec_module(module)  # Actually execute.

                # Can optionally prepend `auto_`, and last one is always done, even if equal to first.
                fun = getattr(module, 'auto_' + tp_lst[-1], getattr(module, tp_lst[-1], None))

        except ModuleNotFoundError as e:  # Didn't find, use default.
            pass

        except BaseException as e:  # Failed entirely.
            print('auto_find_fail', tpstr, type(e), e)
            se = str(e)
            return lambda obj,tpstr, **kv : (SomeError, {}, obj, tpstr, se)

        fun = default_handler if fun is None else fun
        if memoize:
            handlers[clsstr('.'.join(tp_lst))] = fun

        return fun

def is_jspat_obj(obj):
    return getattr(obj, 'jspat_accessible', getattr(obj, 'jspat_get', None))

# TODO `images_dir` and `apparent_dir` are hardcoded.
def auto(obj, check_if_obj=True,
         force_handler=None, handlers=default_handlers, default_handler=no_match_default,
         images_dir="/tmp/jspat/plotlike/",
         apparent_dir="/assets/plotlike/", desc="(missing description)", memoize=False, **kv):
    """Using the type of the given object, uses dictionaries of functions and classes to make
objects."""
    if check_if_obj and is_jspat_obj(obj):  # Already a JSPat object.
        return obj

    Path(images_dir).mkdir(exist_ok=True, parents=True)  # Make the directory if needed.
    tpstr = str(type(obj))
    fun = force_handler or auto_find(tpstr=tpstr,handlers=handlers, default_handler=default_handler,
                                     memoize=memoize)

    cls, kwargs, *rest = fun(obj, tpstr, images_dir=images_dir, **kv)
    if 'basic_info' in kwargs:  # Asks to stuff some basic information in there.
        assert kwargs['basic_info'] == 'y', "`basic_info` is reserved for automatically \
adding some information in keyword arguments of constructors. Must be 'y' or not present."
        kwargs['obj'],kwargs['tpstr']  = obj, tpstr
        kwargs['desc'] = kwargs.get('desc', desc)

    if 'src' in kwargs:  # TODO allow for more of these.
        kwargs['src'] = apparent_dir + kwargs['src']

    assert callable(cls), "BUG: Automatically figuring an object somehow obtained an non-function."
    return cls(*rest, **kwargs)
