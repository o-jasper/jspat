
class C404:
    def __init__(self, path, **q):
        self.path = '/'.join(path)

    def jspat_info(self, **query):
        return {'title':"404 not found: {}".format(self.path), 'http_code':404, 'pattern':'page' }

    jspat_accessible = ['path']

    template_page = ['div.html', "Page {%c|path} not found."]
