#  Copyright (C) 16-01-2019 Jasper den Ouden.
#
#  This is free software: you can redistribute it and/or modify
#  it under the terms of the Affero GNU General Public License as published
#  by the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.

from pathlib import Path

from JSPat.obj.Unit import Unit
from JSPat.obj.Date import Date

lstat_comps = {'mode', 'ino', 'dev', 'nlink', 'uid', 'gid', 'size', 'atime', 'mtime', 'ctime'}

class File:
    """Serves up directories."""
    def __init__(self, path):
        self.po = Path(path)

    def jspat_info(self, **query):
        return {'title':"Directory browsing: {}".format(self.po.as_posix())}

    jspat_by_key = 'file'

    jscall_dir = ['js', """function(cur) {
    var lst = cur.path.split('/')
    lst.shift()
    return lst.join("/")
}"""]
    jscall_file = ['js', """function(cur) {
    var lst = cur.path.split('/')
    return lst[lst.length-1]
}"""]
    jscall_call_exists = ['js', "function(cur, args){ return cur.exists ?  args[1] : args[2] }"]

    # -- JSPat:
    jspat_accessible = ['path', 'exists']
    @property
    def path(self): return self.po.as_posix()

    @property
    def exists(self): return self.po.exists()

    def jspat_get(self, k):
        return (str(getattr(self.po.lstat(), "st_" + k)) if (k in lstat_comps and self.po.exists())
                else "NA")

    def jspat_subgen_dirlist(self, args, q):
        """Generates the sub-directories."""
        if self.po.is_dir(): # TODO defaulty say "empty directory" or "not file" in those cases
            sortby = (args[0] if len(args)>0 else 'ctime')
            return (File(p.as_posix()) for p in
                    sorted(self.po.iterdir(), key=lambda p:getattr(p.lstat(), 'st_' +sortby)))
        else:
            return iter([])

    # -- Plain JSPat that does not refer to other objects in sublists.
    template_page  = ["div.html", """{%need|c|exists}<h3>{%c|path} {%cunit|b|2||size}</h3>{%c|call_exists||<b>Doesnt exist!</b>}<p>{%c|mode} creation time:
 {%ct|date|ctime}, {%ct|hms|ctime}</p>""",
                   "{%sublist|table|dirlist|entry}"]
    template_inline = ["div.html", "{%pat|page}"]

    template_entry = ["tr.htm", """<td>{%c|path}</td><td>{%cunit|b|2||size}</td>
<td>{%ct|if_change_date|ctime}</td><td>{%ct|if_change_hms|ctime}</td>"""]

    # Listing out sub-objects.
    def figure_date(self, which):
        return Date(getattr(self.po.lstat(), "st_" + which))

    lstat_comps = {'mode', 'ino', 'dev', 'nlink', 'uid', 'gid', 'size', 'atime', 'mtime', 'ctime'}
    def jspat_subgen_adate(self, args, q):
        if self.p.exists(): yield self.figure_date('atime')
    def jspat_subgen_mdate(self, args, q):
        if self.p.exists(): yield self.figure_date('mtime')
    def jspat_subgen_cdate(self, args, q):
        if self.p.exists(): yield self.figure_date('ctime')

    def jspat_subgen_size(self, args, q):
        if self.p.exists(): yield Unit(self.po.lstat().st_size, 'b')

    # -- JSPat using sub-objects.
    template_subobj_page  = ["div.html", "<h3>{%c|path}</h3><p>{%c|mode} creation time:{%c|ctime}</p>",
                          "{%sublist|table|dirlist|subobj_entry}"]
    template_subobj_entry = ["tr.htm", "<td>{%c|path}</td><td>{%sublist|div|size|unit_inline}</td>",
                          "<td>{%sublist|div|adate|inline}</td>"]
