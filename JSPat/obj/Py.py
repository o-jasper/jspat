#  Copyright (C) 16-01-2019 Jasper den Ouden.
#
#  This is free software: you can redistribute it and/or modify
#  it under the terms of the Affero GNU General Public License as published
#  by the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.

from JSPat.obj.auto import auto as auto_obj, try_str
import ast

def returns_value(code):
    return code.split()[0] not in {'import', 'class', 'def', 'from'}

def chop_py(code):
    """Chops up python code into the expressions.."""
    parsed = None
    try:  # Split up into its portions.
        parsed = ast.parse("\n".join(code))
    except BaseException as e:
        yield ('parse_error', None, e)
        return
    # The portions are executed by-part.
    ln = 0
    for b in parsed.body[1:]:
        cur_code = code[ln:b.lineno-1]
        yield (returns_value(cur_code[0]) and any(line.find("#(yield)")>=0 for line in cur_code),
               ln, cur_code)
        ln = b.lineno-1

    # Was before line numbers earlier, now need after.
    cur_code = code[ln:]
    yield (returns_value(cur_code[0]) and not any(line.find("#(no yield)")>=0 for line in cur_code),
           ln, cur_code)

class Py:
    def __init__(self, code):
        # TODO might it get query when it shouldnt?
        self.code = code  # Note ignored if query has the goods..

        self.globs, self.locs = {}, {}

    path = "TODO"
    jspat_accessible = ['code', 'path']

    retvar_name = "__py_extract_ret"

    def jspat_subgen_result(self, args, q):
        """Runs the results and yields the last one."""

        for y, ln, code_part in chop_py(q.get('code', self.code).split('\n')):
            # Prepend lines so the reported numbers are right.
            if y:
                exec("\n"*ln + self.retvar_name + " = " + '\n'.join(code_part), self.globs, self.locs)
                yield auto_obj(self.locs[self.retvar_name], default_handler=try_str)
            else:
                exec("\n"*ln + '\n'.join(code_part), self.globs, self.locs)

    template_result = ["div.htm", "{%sublist|table|result|entry}"]
    template_code = [
        "div.htm",
        "{%form|result:result:entry|<textarea cols=80 to_key=\"code\">{%c|code}</textarea><br>",
        "{%submit}}"""]
    template_static_code = ["div.htm", "<pre>{%c|code}</pre>"]

    template_page = ["div.html", "{%pat|code}{%pat|result}"]
    template_inline = ["div.html", "{%pat|code}{%pat|result}"]
