
class SuppliedHTML:
    def __init__(self, html):
        self.html = html

    jspat_accessible = ['html']

    template_inline = ['div.html', "{%c|html}"]
    template_entry  = ['div.html', "{%pat|inline}"]
    template_page = ["div.html", "{%pat|inline}"]

class List:
    def __init__(self, lst, max_cnt=None):
        self.lst = [e for i,e in zip(max_cnt, lst)] if max_cnt else list(lst)

    jspat_accessible = ['len']
    @property
    def len(self): return len(self.lst)

    def jspat_down(self, plst):
        i = int(plst[0])
        if i and i >=0 and i < len(self.lst):
            return self.lst[i], plst[1:]
        else:  # Not found.  # TODO yield a not-found instead.
            return self, []

    def jspat_subgen_list(self, args, q): return self.lst

    template_inline = ['div.html', """<h3>List of {%c|len} items:</h3>
Display using the <code>inline</code> pattern:{%sublist|div|list|inline}"""]
    template_page = ["div.html", "{%pat|inline}"]

class Img:
    def __init__(self, **kv):
        self.data = kv

    def jspat_get(self, k): return self.data.get(k, "{%FC|missing|" + str(k) + "}")

    template_inline = ["div.htm",
                    """<img class="plot_{%c|tpstr}" src="{%c|src}" alt="{%c|desc}">"""]
    template_page = ["div.html", "{%pat|inline}"]
