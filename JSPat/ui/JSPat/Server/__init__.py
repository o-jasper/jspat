# Displayer for the JSPat server itself. Not very useful.

def dumb_html_escape(s): return s.replace('<', "&lt;").replace('>', "&gt;")

class Page:
    def __init__(self, name, get, post):
        self.name = name
        self.get, self.post = dumb_html_escape(str(get)), dumb_html_escape(str(post))

    jspat_by_key = 'name'
    jspat_accessible = ['name', 'get', 'post']  # NOTE get and post yield real pointless stuff..

    template_tr  = ["tr.html", """{%td|{%a|/{%c|name}|{%c|name}}|{%c|get}|{%c|post}}"""]

class Server:
    def __init__(self, server):
        self.server = server

    jspat_accessible = ['server_port', 'server_name']
    # ?serv.address_family
    @property
    def server_port(self): return self.server.server_port
    @property
    def server_name(self): return self.server.server_name

    def jspat_subgen_pages(self, args, q):
        serv = self.server
        lst = sorted(set(serv.post_dict.keys()).union(serv.get_dict.keys()))
        for name in lst:
            yield Page(name, serv.get_dict.get(name, None), serv.post_dict.get(name, None))

    def jspat_down(self, plst):  # TODO incorrect
        serv = self.server
        if len(plst) > 0:
            return (Page(plst[0], serv.get_dict.get(plst[0], None),
                         serv.post_dict.get(plst[0], None)),
                    plst[1])
        else:
            return self, []

    template_page  = ["div.html", """{%h|3|{%c|server_name} on port {%c|server_port}}
Not all links traversable. (TODO mark it..)
{%sublist|table|pages|tr}"""]
    template_inline = ["div.html", "{%pat|page}"]

def auto_Server(val, tpstr, **kv): return Server, {}, val
