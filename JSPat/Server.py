#  Copyright (C) 16-01-2019 Jasper den Ouden.
#
#  This is free software: you can redistribute it and/or modify
#  it under the terms of the Affero GNU General Public License as published
#  by the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.

import json

# Annoyingly, apparently, it doesn't just stop giving material when it reaches
#  the end, gotta get that out of the headers yourself..
def readsplit_maxlen(fd, maxlen, splitby, chunksz=1024):
    """Reads from file, splitting it up and adhering to a maximum length."""
    didsz, got = 0, ""
    while didsz < maxlen:
        sz = min(chunksz, maxlen-didsz)
        got = got + fd.read(sz).decode()
        lst = got.split(splitby)
        for part in lst[:-1]: yield part

        got = lst[-1]
        didsz += sz
    for el in got.split(splitby):
        yield el

def readline_maxlen(fd, maxlen, **keys):
    return readsplit_maxlen(fd, maxlen, '\n', **keys)

import http.server as hs

def split_query(path):
    i = path.find("?")
    return (path,'') if i<0 else (path[:i], path[i+1:])

def figure_query(qstr):
    return { k:"=".join(v) for k, *v in (el.split('=') for el in qstr.split('&')) }

def to_subobj(obj, plst):
    while len(plst)>0:
        obj_fun = getattr(obj, 'jspat_down', None)
        if not obj_fun:
            return obj
        obj, plst = obj_fun(plst)
    return obj

class RequestHandler(hs.BaseHTTPRequestHandler):
    def _type_header(self, http_code, tp):
        """Single header, just the type."""
        self.send_response(http_code)
        self.send_header('Content-type', tp)
        self.end_headers()

    def serve_format(self, httpcode, say, **args):
        """Serve a simple string with formatting."""
        self._type_header(httpcode, "text/plain")  # TODO not right code?
        self.wfile.write(bytes(say.format_map(args), 'utf'))

    chunk_sz = 1024
    def write_fd(self, fd):
        """Write the stream given. Must be a byte-stream."""
        i, wfile, chunk_sz = fd.tell(), self.wfile, self.chunk_sz
        while True:
            wfile.write(fd.read(chunk_sz))
            j = fd.tell()
            if i == j:
                return
            else:
                i = j

    def serve_file(self, code, tp, file):
        try:  # Read, immediately write.
            with open(file, 'rb') as fd:
                self._type_header(code, tp)
                self.write_fd(fd)
        except BaseException as e:
            self.serve_format(
                404, "console.log(\"(Serverside)Couldnt open file; {file}\",\n \"{exception}\")\n",
                file=file, exception=str(e))

    def readline_maxlen(self):
        return readline_maxlen(self.rfile, int(self.headers['Content-Length']))

    def raw_recv(self, dict):  # Figures some stuff out for function below.
        path,qstr = split_query(self.path)
        plst = path[1:].split('/')

        got_name = plst[0] if (plst[0] in dict) else 'default'
        return (plst[1:], qstr, got_name, *dict[got_name])

    def do_GET(self):
        plst, qstr, got_name, handler,obj = self.raw_recv(self.server.get_dict)

        query = figure_query(qstr)
        obj   = (obj(plst,**query) if callable(obj) else obj)
        handler(self, got_name, obj, query)

    def do_POST(self):
        plst, qstr, got_name, handler,obj = self.raw_recv(self.server.post_dict)

        query = json.loads(''.join(self.readline_maxlen()))

        obj = (obj(plst,**query) if callable(obj) else obj)
        obj = to_subobj(obj, query.get('path', []))
        handler(self, got_name, obj, query)

class Server(hs.HTTPServer):

    def __init__(self, addr, handler, dict=None):

        hs.HTTPServer.__init__(self, addr, handler)

        assert dict is not None, "Nothing to serve."

        self.get_dict, self.post_dict  = {}, {}
        self.add_pages(dict)

    def add_page(self, name, handler, obj=lambda p,**q:p):
        if callable(handler):
            self.get_dict[name] = (handler, obj)
        else:
            self.get_dict[name]  = (getattr(handler, 'handle_get', None), obj)
            self.post_dict[name] = (getattr(handler, 'handle_post', None), obj)

    def add_pages(self, dict):
        for name,got in dict.items():
            self.add_page(name, *(got if type(got)==tuple else (got,)))

def memoizing(fun, memoize_into=None):
    """Very basic memoizing from path, ignores url query.
(handler gets query; `JSPat` passes it into `.jstpat_data`)"""
    memoize_into = {} if memoize_into is None else memoize_into
    def ret(p, **q):
        got = memoize_into.get(p)
        if got is None:
            got = fun(p)
            memoize_into[p] = got
        return got
    return ret
