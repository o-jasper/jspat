
// Makes sure there are two numbers.
var two_num = function(str) { str = str + ""; return (str.length >= 2) ? str : ("0" + str); }

var date = function(t){ return new Date(1000*t) }

// NOTE: javascript could not be trusted to correctly do the correct context of the function.
var time_to_str = {
    string : function(t){ return (date(t)).toString(); },
    time   : function(t){ return (date(t)).toTimeString(); },
    date   : function(t){ return (date(t)).toDateString(); },

    locale      : function(t){ return (date(t)).toLocaleString(); },
    locale_time : function(t){ return (date(t)).toLocaleTimeString(); },
    locale_date : function(t){ return (date(t)).toLocaleDateString(); },

    hm  : function(t){
        var d = date(t);
        return d.getHours() + ":" + two_num(d.getMinutes());
    },
    hms_plain : function(t){
        var d = date(t);
        return d.getHours() + ":" + two_num(d.getMinutes()) + ":" +
            two_num(d.getSeconds());
    },
    hms : function(t){
        var d = date(t);
        return "<span class='t_hour'>" + d.getHours()
            + "</span><span class='t_minute'>:" + two_num(d.getMinutes())
            + "</span><span class='t_seconds'>:" + two_num(d.getSeconds()) + "</span>";
    }
}

function time_to_str_if_change(k, t1,t2) { // Only show if particular measure changes.
    var st1 = time_to_str[k](t1), st2 = time_to_str[k](t2)
    if( st1 == st2 ){ return "" }
    else{ return st2 }
}

// Use various relative units.
var tu = ['ms',1, 's',1e3, 'm',60e3, 'h',3600e3, 'd',86400, 'w',7*86400, 'yr',30.4375*86400*12]

function time_to_str_rel_units(t1, t2, step_cnt) {  // Use relative units.
    var dt = Math.abs(t2 - t1)
    if( !t1 || dt == 0 ){ return [] }

    for( var i=0 ; i < tu.length ; i+=2 ){
        if(dt < tu[i+1]) {
            var ret = []
            for( var j=1 ; j<step_cnt && i-2*j>0 ; j++) {
                // division gets the number of tu[i-2*j] and the module makes sure
                // not to add those that are represented by the higher unit.
                ret.push(Math.floor(dt/tu[i-2*j+1])%(tu[i-2*j+1]/tu[i-2*j+3]) + tu[i-2*j])
            }
            return ret.join(' ')
        }
    }
}
