
function ge(name) {
    return document.getElementById(name); 
}

function has_class(cls) {
    return function(el){ return el.classList && el.classList.contains(cls) }
}

function upto(el, crit) {
    if( typeof crit == 'string' ){ crit = has_class(crit) }

    while(!crit(el)) {
        el = el.parentNode;
        if( !el ){ return; }
    }
    return el
}

function downto(el, crit) {  // Finds class depth first.
    if( typeof crit == 'string' ){ crit = has_class(crit) }

    if( !el || crit(el) ) { return el; }

    for(var k in (el.children || [])) {
        var got = downto(el.children[k], crit);  // IMPORTANT Depth first.
        if( got ) { return got }
    }
}

function find_no_esc(cur, find, esc, fr, to) {
    while(true) {
        var i = cur.search(find, fr, to)
        if(i < 0) {  // Didn't find any, report that.
            return i
        } else if( cur.substr(i-esc.length, i) == esc ) {  // Did find, but was escaped.
            fr = i + 1
        } else {  // Did find, good to return.
            return i
        }
    }
}

function once_more(dict, key) { dict[key] = (dict[key] || 0) + 1 }
