//  Copyright (C) 16-01-2019 Jasper den Ouden.
//
//  This is free software: you can redistribute it and/or modify
//  it under the terms of the Affero GNU General Public License as published
//  by the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

// NOTE: console.log will break the "python end"

function raw_pat_call(lst) {
    var ret = [], sub_ret = []
    for( var i=0 ; i<lst.length ; i++ ) {
        if( lst[i] === '' ) {
            // Don't care about empty one. NOTE Do care at level below, then they're arguments.
        } else if( typeof lst[i] == 'string' ) { // String, look for dividers.
            var sub = lst[i].split('|'), join = ""
            for( var j=0 ; j < sub.length-1 ; j++ ) {
                
                if(sub[j].endsWith('\\')) { // Don't divide if escaped, reinsert.
                    sub_ret.push(sub[j] + '|')
                } else {
                    sub_ret.push(sub[j])
                    ret.push(sub_ret)
                    sub_ret = []
                }
            }
            sub_ret.push(sub[sub.length-1])
        } else { // Sub-patterns always just ontop.
            sub_ret.push(lst[i])
        }
    }
    if( sub_ret.length>0 ){ ret.push(sub_ret) }
    return ret
}

var op='{%', cl='}', esc='\\'; // Parsing patterns.
function read_template(cur, depth) {
    var ret = []
    var de_esc = function(s) {
        return s.replace(esc + op, op).replace(esc + cl, cl)
    }
    while(true) {
        var oi = find_no_esc(cur, op, esc)
        var ci = find_no_esc(oi >=0 ? cur.substr(0,oi) : cur, cl, esc)
        if( ci >=0 ) { // Closing was first.
            ret.push(de_esc(cur.substr(0, ci)))
            ret.push(cur.substr(ci+1))  // Return unprocessed string.
            return ret
        } else if(oi >=0) { // Opening was first.
            ret.push(de_esc(cur.substr(0, oi)))
            cur = cur.substr(oi+2)
            
            var got = read_template(cur, depth+1)
            cur = got.pop()
            ret.push(raw_pat_call(got))
        } else {  // No more patterns.
            ret.push(cur)
            ret.push("")
            return ret
        }
    }
}

function use_pat(pat, funs, data, ret) { // Applying patterns.
    var ret = ret || []
    if( typeof pat == 'string' ) { // Don't do anything with plain strings.
        if(pat.length>0){ ret.push(pat) }
    } else {
        for( var i=0 ; i < pat.length ; i++ ) { // List of parsed patterns.
            var cur = pat[i]
            if( typeof cur == 'string' ) {  // Just a plain string to enter.
                if(cur.length>0){ ret.push(cur) }
            } else {  // A pattern to interpret.
                var call_name = cur[0].join('')
                var args = []
                for( var j=1 ; j<cur.length ; j++ ) {
                    args.push(use_pat(cur[j], funs, data).join(''))
                }
                var got = (funs[call_name] || funs['default'])(call_name, args, data, ret.length)
                // Recurse.(might get string and just end up being plainly that.)
                use_pat(got, funs, data, ret)
            }
        }
    }
    return ret
}
function use_call(data, args, cur) {
//    once_more(data.need_call, args[0])

    var fname = data.cur_pat_name.split('/')[0] + '/' + args[0]
    var fun = data.calls[fname]
    if(fun) {
        return fun(cur || data.cur, args)
    } else {  // Missing call/component.
        return "{MC%|" +fname + '|' + args.join('|') + "}"
    }
}

function use_c(data, args, cur) {  // Get/calculate piece of information from data.
    once_more(data.need_c, args[0])

    var cur = cur || data.cur
    if( args.length == 1 ) { // Single value
        var got = cur[args[0]]
        if( got === undefined ){  // Calculate the value.
            got = use_call(data, args, cur)
        }
        cur[args[0]] = got
        return got
    } else {
        return use_call(data, args, cur)
    }
}

// Decodes an argument list to a dictionary that pairs them with their names.
function decode_1(pat, got) {
    var ret = {}, need_c = pat[2]
    for( var i=0 ; i < need_c.length ; i++ ) { ret[need_c[i]] = got[i+1] }
    return ret
}

function eval_call(name, code) { return eval("(" + code + ")") }

// Absorbing commands, where it doesn't matter if to-objects or to-string.
function obtain_1_rest(got, funs, data) {
    switch(got[0]) {
    case 's': case 'set_pattern':  // Receiving a pattern.
        // TODO also permit bunch of functions related just-to the object?
        // list is: (name, pattern, component_order, by_key)
        data.patterns[got[1]] = [got[2], read_template(got[3], 0), got[4], got[5]]
        break
    case 'k': case 'set_call':  // TODO detect string for just equating to existing one.
        //if( (data.permissions || {}).set_call ) {
        data.calls[got[1]] = eval_call(got[1], got[2])
        //}
        break
    case 'u': case 'use_pattern':  // Which thing we're filling in.
        data.cur_pat_name = got[1]
        data.cur_pat = data.patterns[got[1]]
        break
    case 'd': case 'exit_name':
        var now = data.into_stack.pop()
        data.into_el = now[0]
        data.cur_index = now[1]
        break
    case 'c':
        data.prev = data.cur
        data.cur  = decode_1(data.cur_pat, got)
        data.cur_index += 1
        break
    case ',': case 'next':  // Move to next place to place output.
        if(data.next_fun){ data.next_fun(got, data) }
        break
    case 'p': case 'permissions':
        if( data.permissions === undefined ){
            data.permissions = got[1]
        } else if(console.error) {
            console.error("Permissions doubly specified, latter ignored")
        }
        break
    case 'dud':
        break
    default:
        if( console.error ){ console.log("Obtained, but don't recognize:", got) }
        break
    }
}

// Absorb a piece, make a single list.
function obtain_1_solid(got, funs, data) {
    if(!got) { return }
    var ret = data.into_el

    switch(got[0]) {
    case 'n': case 'on_name':  // Which place we're filling in.
        data.into_stack.push([data.into_el, data.cur_index])
        data.into_el = [got[1]]
        ret = data.into_el

        // Assert apparently currently needed things.
        //if( !({'o':1, 'overwrite':1})[got[2][0]] || !({'append':1, 'a':1})[got[2][1]] ) {
            //console.log("Only appending, overwriting with solid pieces.")
        //}
        break
    case 'd': case 'exit_name': // Put in the current.
        var name = ret[0], place = ret

        obtain_1_rest(got, funs, data)
        ret = data.into_el
        var i = ret.length-2

        var part = "" + ret[i]  // NOTE w/o "" it says `part` not defined?
        var search_for = " class=\"sublist sl_" + name + "\">"
        var j = part.search(search_for) + search_for.length
        var pre = part.substr(0,j), post = part.substr(j)

        place.shift()
        ret[i] = pre + place.join('') + post  // Modify relevant entry.
        break
    case 'c': case 'continue':  // Continue filling in.
        obtain_1_rest(got, funs, data)

        ret.push("<" + data.cur_pat[0] + ">")  // Run it, with little wrap.
        ret.push(use_pat(data.cur_pat[1], funs, data).join(''))
        ret.push("</" + data.cur_pat[0] + ">\n")
        break
    case  'error':
        ret.push("\n<br>Got error " + JSON.stringify(got) + "\n<br>")
        break
    default:
        obtain_1_rest(got, funs, data)
        break
    }
}

// Absorb one piece of data.
function obtain_1(got, funs, data) {
    if(!got) { return }
    switch(got[0]) {
    case 'n': case 'on_name':  // Which thing we're filling in.
        data.into_stack.push([data.into_el, data.cur_index])
        data.into_el = downto(data.into_el.lastChild, 'sl_' + got[1]) || data.into_el
        if( ({'o':1, 'overwrite':1})[got[2][0]] ) {
            data.into_el.innerHTML = ""
        }
        data.into_mode = ({'prepend':'p', 'append':'a'})[got[2][1]] || got[2][1] || 'a'
        data.cur_index = 0
        break
    case 'sm': case 'set_mode':
        if( ({'o':1, 'overwrite':1})[got[1][0]] ) {
            data.into_el.innerHTML = ""
        }
        data.into_mode = ({'prepend':'p', 'append':'a'})[got[1][1]] || got[1][1] || 'a'
        break
    case 'c': case 'continue':  // Continue filling in.
        obtain_1_rest(got, funs, data)

        var el = document.createElement(data.cur_pat[0])
        el.className = 'jspat_el'

        var by_key = data.cur_pat[3]  // Add the key.
        el.setAttribute('jspat_key', by_key==':index' ? data.cur_index - 1 : use_c(data, [by_key]))

        el.innerHTML = use_pat(data.cur_pat[1], funs, data).join('')

        data.sub_el = el
        if(data.into_mode == 'a'){
            data.into_el.appendChild(el)
        } else {
            // data.into_el.prependChild(el)
            data.into_el.insertBefore(el, data.into_el.childNodes[0]);
        }
        break
    case '+': case 'add_code':  // TODO limitation: only happens in browser.
        if( (data.permission || {}).add_code ){
            var el = document.createElement('script')
	    el.setAttribute("type","text/javascript")
	    el.setAttribute("src", got[1])
            document.head.appendChild(el)
        }
        break
    case 'error':
        console.log('server sent error:', got)
        break
    default:
        obtain_1_rest(got, funs, data)
        break
    }
}

function plain_pattern(pattern, inputspec) {
    var pat = (typeof pattern == 'string' ? read_template(pattern, 0) : pattern)
    return function(cn, args) {
        var use_funs = { 'default':function(scn){
            var spec = inputspec[scn]
            if(typeof spec=='number') {
                return args[spec] || ""
            } else if(typeof spec=='function') {
                return spec(args)
            } else {
                for( var i=0 ; i<spec.length ; i++ ) {
                    if( args[spec[i]] ) { return args[spec[i]] }
                }
            }
        }}
        return use_pat(pat, use_funs, {})
    }
    
}
function constant_fun(what){ return function(){ return what } }

function init_data(data) {  // Starts up some of the dictionaries, counters and lists.
    data = data || {}

    data.cur_pat_name = "WARN:NOTSET"
    data.calls = {}
    data.into_stack = (data.into_el ? [data.into_el] : [])
    data.cur = {}; data.prev = {}

    data.need_template = {}; data.need_c = {};  data.sublists_cnt = 0;data.sublists = {}
    data.patterns = {}; data.need_call = {} ; data.need_script = {}
    return data
}

function tag_w_args_pattern(tag) {
    return plain_pattern("<" + tag + "%a}>{%b}</" + tag + ">",
                         {'a':function(a){ return a.length>0 ? " " + a[0] : ""}, 'b':[1,0] })
}

form_submit = null

glob_funs = {
    'a' : plain_pattern("<a href=\"{%url}\"{%args}>{%body}</a>",
                        {'url':0, 'args':function(a){ return a.length>2 ? " " + a[1] : "" },
                         'body':[2,1,0] }),
    'h' : plain_pattern("<h{%n}{%args}>{%body}</h{%n}>",
                        {'n':0, 'args':function(a){ return a.length>2 ? " " + a[1] : "" },
                         'body':[2,1,0] }),
    'td' : function(cn, args) {
        return "<td>" + args.join("</td><td>") + "</td>"
    },

    'c' : function(cn, args, data) { return "" + use_c(data, args) },

    'ct' : function(cn, args, data) {  // Component as time.
        var pargs = []
        for( var i=1 ; i<args.length ; i++ ){ pargs.push(args[i]) }

        var which = "" + args[0]
        var fun = time_to_str[which]
        if(fun) { return fun(use_c(data, pargs)) }

        var t1 = use_c(data, pargs, data.prev), t2 = use_c(data, pargs)
        if( which.startsWith('if_change_') ) {
            return time_to_str_if_change(which.substr(10), t1,t2)
        } else if( which.startsWith('rel_units_') ){
            return time_to_str_rel_units(which.substr(10), t1,t2)
        }
        return "{FCT|" + args.join('|') + '}'
    },
    'cunit' : function(cn, args, data) {  // Component numerical, with unit added.
        var pargs = []
        for( var i=3 ; i<args.length ; i++ ){ pargs.push(args[i]) }
        return val_unit(use_c(data, pargs), args[0] || '', parseInt(args[1])||1, args[2])
    },

    'fail' : function(cn, args) { return "{F%" + cn + '|' + args.join('|') + "}" },
    'default' : function(cn, args) {
        var ret = [data.calls[cn] ? 'c' : 'fail']
        for(var i=0 ; i<args.length ; i++) { ret.push(args[i]) }
        return [ret]
    },
    'sublist' : function(cn, args, data) { // html_tag, pattern_name, subgen_name, *other_args
        data.sublist_cnt += 1

        var info = []
        for(var i = 1 ; i < args.length ; i++){ info.push(args[i]) }
        if( console.error ){ console.log(cn, args, data.cur_pat) }  // TODO.. uh its different in browser.
        // Remember this will be infillable later.
        data.sublists[data.cur_pat + ':' + args[1]] = info  // Stored on the pattern name.
        var tag = args[0] || 'div'
        return "<" + tag + " class=\"sublist sl_" + args[1] + "\"></" + tag.split(' ')[0] + ">"
    },

    // Manually indicate some things are needed.(currently not really needed)
    'need' : function(cn, args, data) {
        if( !({'call':1, 'pat':1, 'c':1, 'script':1})[args[0]] ){
            return "{WI%bad input; " + args[0] + "; can only %need pat, call, script, or c}"
        }
        var dict = data["need_" + args[0]]
        for( var i=1 ; i<args.length ; i++ ){ once_more(dict, args[i]) }
        return ""
    },

// Including/needing other patterns.
    'pat' : function(cn, args, data) {  // Use a named pattern.
        once_more(data.need_template, args[0])
        var fname = data.cur_pat_name.split('/')[0] + '/' + args[0]
        return (data.patterns[fname] || [])[1] || "{FP%|Dont have pat?|" + fname + "}"
    },
// Fillable forms that update sublist(s) // NOTE: needs an submit to actually submit.
    'form' : function(cn, args, data) {
        once_more(data.need_script, "form.js")
        if(form_submit) {
            return "<div into='" + args[0] + "' class='form'>" + args[1] + "</div>"
        } else {  // NOTE: it might just be exploring what is needed for the pattern.
            return "<div class='noform'>Forms not available/not permitted.</div>"
        }
    },
    'submit' : function(cn, args, data) {
        if( form_submit ) {
            return "<button class='submit' onclick='form_submit(glob_data, upto(this, \"form\"))'>"
                + (args[0] || "submit") + "</button>"
        } else {
            return "<div class='noform error'>You shouldn't see this. " +
                "Either JSPat or an showable instance has a bug.</div>"
        }
    }
}
