resp_from_this = true;

var dumb_parse = function(str) {
    return JSON.parse("[" + str + "]")[0];  // Super necessary and smart.
}

function state_change_cb(get_ret, final_fun) {
    if( !get_ret ) { return function(){}; }

    var at_i = 1;
    return function(resp) {
        resp = resp_from_this ? this : resp;

        var rt = resp.responseText;
        while(true) {  // Regular chunks, delimited by ,\n
            var len = rt.substr(at_i).search(",\n");
            if( len < 0 ){ break; } // No more.

            get_ret(dumb_parse(rt.substr(at_i, len).trim()), resp.status)
            at_i += len + 2;
        }
        if(resp.readyState == 4) {  // Final chunk.
            var json = rt.substr(at_i)
            json = json.substr(0, json.search("\n]")).trim()  // Indicates the end.
            get_ret(dumb_parse(json), resp.status);

	    if( final_fun ) { final_fun(resp.status); }
        }
    }
}

function msg_get(url, get_ret, final_fun) {
    var req = new XMLHttpRequest()
    req.overrideMimeType("application/json")

    req.onreadystatechange = state_change_cb(get_ret, final_fun)
    req.open('GET', url, true)
    req.setRequestHeader("Content-type", "text/json")
//    req.setRequestHeader("Cache-Control", "no-cache");

    req.send()
}

function msg_post(url, data, get_ret, final_fun) {
    var req = new XMLHttpRequest()
    req.overrideMimeType("application/json")

    req.onreadystatechange = state_change_cb(get_ret, final_fun)
    req.open('POST', url, true)
    req.setRequestHeader("Content-type", "text/json")
    req.setRequestHeader("Cache-Control", "no-cache");

    req.send(data)  // TODO what about sending bit by bit too?
}
