// Please keep in mind, people want to use their keyboards.
//  (try use keys not already used) TODO find a core "dont thread" list?
keybind_keycode_dict = {
    'right':39, 'left':37, 'up':38, 'down':40,
    'backspace':8, 'enter':13,'return':13, 'esc':27,
    'del':46, 'delete':46, 'tab':9, 'capslock':20, 'numlock':144,
    'space':32,
    'f1':112, 'f2':113, 'f3':114,  'f4':115,  'f5':116, 'f6':117,'f7':118,
    'f8':119, 'f9':120, 'f10':121, 'f11':122, 'f12':123,
    'click':'click'
}

keycode_keybind_dict = {}
for(var k in keybind_keycode_dict) {
    if( k.toLowerCase()!= k ){ console.error("BUG: keycode description " + k + " not lowercase.") }
    keycode_keybind_dict[keybind_keycode_dict[k]] = k
}

keytypes_sole = { 'click':'click' }

function new_bindings(dict){ dict.usable = 'yes'; return dict }

function event_binding_str(evt) {
    if( evt.key && evt.key.length == 1 ){ return (evt.ctrlKey ? "C-" : "") + evt.key }

    return (evt.shiftKey ? "S-" : "") + (evt.ctrlKey ? "C-" : "") +
        (keytypes_sole[evt.type] || keycode_keybind_dict[evt.keyCode] || evt.key)
}
    
// Given bindings and a list of objects, finds the first object with a suitable function/method.
var find_1_act = function(objects, by_name) {
    for( var i in objects ) { if(objects[i][by_name]){ return objects[i]; } }
}

function utilize_bindings(bindings, objects, args, pre, post, prep='act_') {
    if(bindings.usable!='yes'){ console.error("Bindings were not enabled.") }
    args = args || {}

    return function input_kd(el, evt) {
        if(pre) { el = pre(el, evt, args) || el }

        var estr = event_binding_str(evt)
        var act_name = bindings[estr] || bindings[estr.substr(2)] || bindings[estr.substr(4)]
            || bindings.otherwise
        if(typeof(act_name)=='string') {  // Single action.
            find_1_act(objects, prep + act_name)[prep +act_name](el, evt, args);
        } else if( act_name ) {  // List of actions.
            for( var i in act_name ) {
                find_1_act(objects, prep + act_name[i])[prep +act_name[i]](el, evt, args);
            }
        }
        if(post){ post(el, evt, args, act_name); }
    }
}
