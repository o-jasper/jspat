//  Copyright (C) 16-01-2019 Jasper den Ouden.
//
//  This is free software: you can redistribute it and/or modify
//  it under the terms of the Affero GNU General Public License as published
//  by the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

// Find input-like things and pput into a dictionary using the `to_key` attribute of the tags.
function form_extract_values(base) {
    var tags = ['INPUT', 'TEXTAREA'], ret = {}
    for( var j=0 ; j<tags.length ; j++ ) {
        var lst = base.getElementsByTagName(tags[j])
        for( var i=0 ; i<lst.length ; i++ ) {
            var key = lst[i].getAttribute('to_key')
            if( key == 'path' || key == 'mode' || key == 'sublists' ) {
                console.log("This key for form is ignored because it is reserved:", key)
            } else if(key) {
                ret[key] = lst[i].value
            }
        }
    }
    return ret
}
function extract_path(el) {
    var ret = []
    while(el) {
        while( !el.classList || !el.classList.contains('jspat_el') ) {
            el = el.parentNode
            if(!el){ return ret }
        }
        ret.push(el.getAttribute('jspat_key'))
        el = el.parentNode
    }
    return ret
}

// `into` is a list of strings which split by ':' get
//  [into_name, subgen_name, pattern_name, write_mode] 
function form_submit(data, values_base, into) {
    if( into===null || into===undefined ) {
        into = values_base.getAttribute('into').split(',')
    }

    data.do_after = data.do_after || [] // If sequence of actions, this will list it.

    var ret = form_extract_values(values_base)

    ret.sublists = ""
    for( var i=0 ; i<into.length ; i++ ) {  // Subgens to return with.
        var got = into[i].split(':')
        ret.sublists += (i!=0 ? ',' : "") + got[1] + ':' + got[2]
    }
    
    var base = values_base // Down to base.
    while( base.parentNode.classList && !base.parentNode.classList.contains('sublist') ){
        base = base.parentNode
    }
    ret.path = extract_path(base)

    var use_url = document.URL  // Just uses the straight url.

    var do_it = function() {
        data.not_ready = true  // Reserve it.

        var i = 0;
        data.next_fun = function() {  // How to go to next case.
            var got = (into[i] || into[into.length-1]).split(':')
            var el = downto(base, "sl_" + got[0])
            data.into_el = el; data.into_stack = [el]
            // Override and append, otherwise force append(TODO)
            obtain_1(['sm', into[i] ? (got[3] || 'oa') : 'ia'], null, data)
            i++
        }

        var cmd_i = 0
        // Actually send.  (POST because i don't want it to be on the same channel 
        msg_post(use_url, JSON.stringify(ret),
                 function(got) {
                     if(cmd_i==0) { data.next_fun() } // Get started on first.
                     cmd_i += 1
                     obtain_1(got, glob_funs, data)
                 },
                 function() {
                     data.next_fun = null  // Reset next fun.
                     if(data.do_after.length > 0){ data.do_after.shift()() }
                     else { data.not_ready = false } // Unreserve it.
                 })
    }
    if( data.not_ready ) {
        data.do_after.push(do_it)
    } else {
        do_it()        
    }
}
