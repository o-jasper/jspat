
function mov_fun(from_el, mov) {
    from_el = from_el && upto(from_el, 'jspat_el')  // Current element.
    if( !from_el ) { return }  // Ran out completely, there's no next.

    var iter = mov(from_el)
    while( iter && !iter.classList.contains('jspat_el') ) { iter = mov(iter) }
    if( !iter ) { return mov(upto(from_el.parentNode, 'jspat_el')) }  // Ran out, go up.

    return iter // Found it.
}

function mov_next(from_el) { return mov_fun(from_el, function(e){ return e.nextSibling }) }
function mov_prev(from_el) { return mov_fun(from_el, function(e){ return e.previousSibling }) }

// TODO/NOTE: while it goes "up" after the first/last, it never goes "down"

// TODO UI functions based on it.
function el_in_view(el) {
    var top = el.offsetTop,     left = el.offsetLeft;
    var width = el.offsetWidth, height = el.offsetHeight;

    while(el.offsetParent) {
        el = el.offsetParent;
        top += el.offsetTop; left += el.offsetLeft;
    }

    return (
        top >= window.pageYOffset &&
        left >= window.pageXOffset &&
        (top + height) <= (window.pageYOffset + window.innerHeight) &&
        (left + width) <= (window.pageXOffset + window.innerWidth)
    )
}

current = null;
function change_current(to, opts) {
    if(!to) { return }
    if( current ) {
        current.style = undefined
        if( opts.scroll ){ window.scrollBy(0, to.offsetTop - current.offsetTop) }
    }
    to.style = "background-color:grey"
    current = to
}

function best_el(lst, scorer, filter) {
    var best_score=null, best=null
    for( var i=0 ; i<lst.length ; i++ ) {
        if(!filter || filter(lst[i])) {
            var score = scorer(lst[i])
            if( !best || score > best_score ) {
                best_score = score
                best = lst[i]
            }
        }
    }
    return best
}

class BasicKb {
    act_prevent_default(el, evt) { evt.preventDefault(); }
    act_refocus(el){ el.focus() }
}

function is_inputlike(el) { return ({'TEXTAREA':1, 'INPUT':1, 'BUTTON':1})[el.tagName] }

class BrowseActions extends BasicKb {
    act_next() {
        change_current(mov_next(current), {'scroll':1})
    }  // Next/previous.
    act_prev() {
        change_current(mov_prev(current), {'scroll':1})
    }
    act_in() {  // Go into object.
        var children = current.children
        for( var i in children ) {
            var got = downto(children[i], 'jspat_el')
            if(got) { return change_current(got, {}) }
        }
    }
    act_into_form(el, evt) {
        var form = downto(current, 'form')
        if(form) {
            var got = downto(form, is_inputlike)

            if(got){
                this.act_prevent_default(el, evt) // Don't immediate do the things.
                got.focus()
                mode['current'] = mode['form']  // Change to form mode.
            }
        }
    }
    act_out() {  // Get out of object.
        change_current(upto(current.parentNode, 'jspat_el'), {})
    }

    act_get_in_view() {
        if( current && !el_in_view(current) ) {
            current.scrollIntoView()
            window.scrollBy(0, -50)
        }
    }
    act_find_initially() { this.act_find_in_view() }
    act_find_in_view() {
        if( !current || !el_in_view(current) ) {
            var from = current || { 'offsetTop' : window.pageYOffset + window.innerHeight/4 }
            
            var got = best_el(document.body.getElementsByClassName('jspat_el'),
                              function(el){ return -Math.abs(from.offsetTop - el.offsetTop) },
                              el_in_view)
            
            return got && change_current(got, {})
        }
    }
}
class FormActions extends BasicKb {  // TODO this guy needs to be traversable aswel.
    act_esc() { mode['current'] = mode['browse'] }

    act_submit() {
        var form = downto(current, 'form')
        if(form) { form_submit(glob_data, form) }
    }
}

var mode = {
    'browse' : [new BrowseActions()],
    'form'   : [new FormActions()],
}
mode['current'] = mode['browse']

mode['browse'].bindings = {
    'up'    : ['prev', 'get_in_view', 'prevent_default'],
    'down'  : ['next', 'get_in_view', 'prevent_default'],
    'left'  : ['out' , 'get_in_view', 'prevent_default'],
    'right' : ['in',   'get_in_view', 'prevent_default'],
    'return' : ['into_form'],
}
mode['form'].bindings = {
    'esc' : ['esc'],
    "C-return" : ['submit']
}
document.onkeydown = function(evt){
    if(current) {  // TODO bindings may need to vary with mode too..
        utilize_bindings(new_bindings(mode['current'].bindings), mode['current'])(null, evt)
    } else { mode['browse'][0].act_find_initially() }
}

window.addEventListener('wheel', //'scroll', or window.onscroll also react to .scrollBy
                        function(){
                            if(mode['current'] == mode['browse']) {
                                mode['browse'][0].act_find_in_view()
                            }
                        },
                        true)
