
var default_seq = { 0:' ', 3:'k', 6:'M', 9:'G', 12:'T' }
default_seq[-9] = 'n'; default_seq[-6] = 'mu'; default_seq[-3] = 'm'

function val_unit(val, unit, dec, e, seq) {
    var n = Math.floor(val==0 ? 0 : Math.log10(Math.abs(val))/3)
    var x = Math.round(val*10**(dec-3*n))*10**-dec

    var got = !e && (seq || default_seq)[3*n]
    return ('' + x).substr(0, dec+2) + (got || (n!=0 ? 'E' + 3*n : '')) + unit
}
